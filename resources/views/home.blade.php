@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header col-md-12">
                    <span>Product List</span>
                    <span class="float-right">
                        <a class="btn btn-success" href="{{route('products.index')}}"><i class="fa fa-plus"></i></a>
                    </span>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <table id="products" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Created On</th>
                                <th width="140px">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($product_list as $product)
                                <tr>
                                    <td>
                                        @if($product->image)
                                            <img width="50px" height="50px" src="{{ asset('images/'.$product->image) }}">
                                        @endif
                                    </td>
                                    <td>{{$product->name}}</td>
                                    <td>{{($product->category) ? $product->category->name : ''}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>{{$product->quantity}}</td>
                                    <td>{{$product->created_at->format('M d Y H:i A')}}</td>
                                    <td>
                                        <a class="btn btn-success float-left mr-2" href="{{ route('products.edit',$product->id) }}"> Edit</a>
                                        <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button class="btn btn-danger"> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Created On</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#products').DataTable({
                "aaSorting": [],
                "columnDefs": [
                    { "orderable": false, "targets": [0, 6] }
                ],

                "processing": true,
                "serverSide": true,
                "initComplete":function(){
                    // onint();
                    },
                ajax:'{{route('get-products')}}',
                "columns": [
                    { "data": 0 },
                    { "data": 1, name:'products.name' },
                    { "data": 2, name:'category.name' },
                    { "data": 3, name:'products.price' },
                    { "data": 4, name:'products.quantity' },
                    { "data": 5, name:'products.created_at' },
                    { "data": 6 },
                ]
            });
        } );
    </script>
@endsection