@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header col-md-12">
            <span>@if($edit) Product : {{$product->name}}@else New Product @endif</span>
            <span class="float-left mr-3">
              <a href="{{route('home')}}"><i class="fa fa-arrow-left"></i></a>
            </span>

            <span class="float-right">
              <button class="btn btn-success" data-toggle="modal" data-target="#CategoryModel">
                <i class="fa fa-plus"></i> Add category
              </button>
            </span>
          </div>

          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif

            {!! Form::model($product, ['url' => route('products.store',$product->id), 'files' => true])  !!}
              @if($edit) {{ Form::hidden('id',$product->id) }} @endif
              <div class="form-group mb-3">
                <div class="col-md-6 float-left">
                  {!! Form::label('name', 'Name') !!}
                  {!! Form::text('name', $product->name, ['class' => 'form-control']) !!}
                </div>

                <div class="col-md-6 float-left">
                  {!! Form::label('price', 'Price') !!}
                  {!! Form::text('price', $product->price, ['class' => 'form-control']) !!}
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 float-left">
                  {!! Form::label('quantity', 'Quantity') !!}
                  {!! Form::number('quantity', $product->quantity, ['class' => 'form-control']) !!}
                </div>

                <div class="col-md-6 float-left">
                  {!! Form::label('category_id', 'category') !!}
                  {!! Form::select('category_id', $category_list, $product->category_id, ['class' => 'form-control']) !!}
                </div>
              </div>
              <div class="clearfix mb-3"></div>
              <div  class="form-group">
                <div class="col-md-6 float-left">
                  {!! Form::label('image', 'Product Image') !!}
                  {!! Form::file('image', ['class' => 'image form-control']) !!}
                </div>
                @if($product->image)
                  <div class="image-wrapper d-inline-block text-center img-thumbnail">
                    <img src="{{asset('images/'.$product->image)}}">
                  </div>
                @endif
              </div>

              <div class="clearfix mb-3"></div>

              <div class="form-group col-md-12">
                <button class="btn btn-success" type="submit">@if($edit) Update Product @else Add Product @endif</button>
              </div>

              {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- New category model -->
  <div id="CategoryModel" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <span class="modal-title">New Category</span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group mb-3">
            <div class="col-md-12 float-left">
              {!! Form::label('category_name', 'Category Name') !!}
              {!! Form::text('category_name', '', ['class' => 'form-control','required' => true]) !!}
              <div class="alert-danger" style="display: none;" id="cat_available_error" ><span>Category name is already available.</span></div>
            </div>
          </div>
          <div class="clearfix mb-3"></div>

          <div class="form-group col-md-12">
            <button class="btn btn-success" type="button" onclick="addCategory()">Add Category</button>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  function addCategory() {
      let categoryName = $('#category_name').val();
      if(!categoryName){
          $('#category_name').addClass('has-error');

          $.notify("Category name is required!", "error");
          return false;
      }
      $.ajax('{{ route('add-category') }}',
          {
              type: 'POST',  // http method
              data: { name: categoryName, _token: '{{csrf_token()}}' },  // data to submit
              dataType: 'json', // type of response data
              timeout: 2000,     // timeout milliseconds
              success: function (data,status,xhr) {   // success callback function
                  $('#CategoryModel').modal('toggle');
                  $('#category_id').empty();
                  $.each(data.category_list, function( index, value ) {
                      $('#category_id').append($('<option>', {value:index, text:value}));
                  });
              },
              error: function (error) { // error callback
                $('#cat_available_error').show();
              }
          });
  }
</script>
@endsection