<?php

namespace App\Http\Controllers;


use App\Products;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the product list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //factory(Category::class, 20)->create(); // code for generate fake entries
        //factory(Products::class, 200)->create(); // code for generate fake entries
        $product_list = Products::with('category')->orderBy('created_at','DESC')->get();
        return view('home', compact('product_list'));
    }
}
