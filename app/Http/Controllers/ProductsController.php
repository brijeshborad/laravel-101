<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = new Products();
        $edit = false;
        $category_list = Category::pluck('name','id')->toArray();
        return view('products.index', compact('product','edit','category_list'));
    }

    /**
     * Add or Edit Product.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'quantity' => 'required',
            'category_id' => 'required',
            'price' => 'required|numeric',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()
                ->withInput()
                ->with('error',$validator->errors()->first());
        }

        $product_obj = Products::find($request->id); // Update Product
        $success_message = 'Product detail updated.';
        if(count((array)$product_obj) == 0){
            $product_obj = new Products(); // New Product
            $success_message = 'Successfully added a product';
        }

        $image_name = $product_obj->image;
        if($request->file('image')){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $input['imagename']);
            $image_name = $input['imagename'];
        }

        $product_obj->image = $image_name;
        $product_obj->name = $request->get('name');
        $product_obj->price = $request->get('price');
        $product_obj->category_id = $request->get('category_id');
        $product_obj->is_active = ($request->is_active != null) ? 1 : 0;
        $product_obj->quantity = $request->get('quantity');

        if($product_obj->save()) {
            return redirect()->route('home')
                ->with('success', $success_message); // You may print this success message
        } else {
            return redirect()->back()
                ->withInput()
                ->with('error', 'Could not add a product');      // You may print this error message
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Products::find($id);
        if(count((array)$product) > 0){
            $edit = true;
            $category_list = Category::pluck('name','id')->toArray();
            return view('products.index', compact('product','edit','category_list'));
        } else {
            return redirect('/home')->with('error', 'Product not found');      // You may print this error message
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::find($id);
        if(count((array)$product) > 0){
            $product->delete();
            return redirect('/home')->with('success', 'Product deleted.');
        } else {
            return redirect('/home')->with('error', 'Product not found');      // You may print this error message
        }
    }

    /**
     * Add new category and return new category list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addCategory(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:category'
        ]);
        $category_obj = new Category();
        $category_obj->name = $request->name;
        $category_obj->save();
        $category_list = Category::pluck('name','id')->toArray();

        return response()->json(['status'=>'success','category_list' => $category_list]);
    }

    public function getProducts(Request $request){
        $sort_field_name = 'products.updated_at';
        $sort_type = 'asc';
        if(is_array($request->order) && count($request->order) > 0){
            $sort_array = ($request->order)[0];
            $sort_field_name = ($request->columns)[$sort_array['column']]['name'];
            $sort_type = $sort_array['dir'];
        }

        $search_string = null;
        $search_arr = $request->get('search',[]);
        if(isset($search_arr['value'])){
            $search_string = $search_arr['value'];
        }
        $product_list = [];

        $product_list_obj = Products::select(['products.*','category.name as category_name'])
            ->leftJoin('category as category','category.id','=','products.category_id')
            ->where(function($q) use ($search_string){
                if($search_string != null){
                    $q->orWhere('products.name','LIKE','%'.$search_string.'%');
                    $q->orWhere('products.price','LIKE','%'.$search_string.'%');
                    $q->orWhere('products.quantity','LIKE','%'.$search_string.'%');
                }
            });
             if($search_string != null) {
                 $product_list_obj->orWhereHas('category',function($qc) use ($search_string){
                     return $qc->where('category.name', 'LIKE', '%' . $search_string . '%');
                 });
             }
        ;

        $total_product = $product_list_obj->count();
        $product_list_data = $product_list_obj->skip($request->get('start',0))
            ->take($request->get('length',10))
            ->orderBy($sort_field_name,$sort_type)
            ->get()
        ;

        foreach($product_list_data as $product){
            $product_list[] = [
                ($product->image) ? '<img width="50px" height="50px" src="'.asset('images/'.$product->image).'">' : '',
                $product->name,
                $product->category_name,
                $product->price,
                $product->quantity,
                date('M d Y H:i A',strtotime($product->created_at)),
                '<a class="btn btn-success float-left mr-2" href="'.route('products.edit',$product->id).'"> Edit</a>'.
                '<form action="'.route('products.destroy',$product->id) .'" method="POST">'.
                method_field('DELETE').
                csrf_field().
                '<button class="btn btn-danger"> Delete</button></form>'

            ];
        }

        $response = [
            'draw'=>$request->draw,
            'recordsTotal' => $total_product,
            'recordsFiltered' => $total_product,
            'data' => $product_list
        ];
        return response()->json($response);
    }
}
