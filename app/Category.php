<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'category';

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
