<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Products;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Products::class, function (Faker $faker) {
    $categories = \App\Category::pluck('id')->toArray();
    return [
        'name' => $faker->name,
        'price' => rand(0,2000),
        'quantity' => rand(0,2000),
        'category_id' => $faker->randomElement($categories),
        'is_active' => rand(0,1),
        'image' => null
    ];
});
