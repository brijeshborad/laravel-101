<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('products', 'ProductsController')->except(['create', 'show', 'update']);

Route::post('add-category', 'ProductsController@addCategory')->name('add-category');
Route::get('get-products', 'ProductsController@getProducts')->name('get-products');